<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Http\Controllers\Auth\Controller;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//# DASHBOARD
Route::get('/', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::get('/dashboard', [App\Http\Controllers\DashboardController::class, 'index'])->name('dashboard')->middleware('auth');

//# USER
Route::get('user/list', [App\Http\Controllers\UserController::class ,'list'])->name('user list')->middleware('auth');
Route::post('user/add', [App\Http\Controllers\UserController::class, 'add'])->name('user add')->middleware('auth');
Route::get('user/edit/{id}', [App\Http\Controllers\UserController::class, 'editform'])->name('user edit')->middleware('auth');
Route::post('user/edit/{id}', [App\Http\Controllers\UserController::class, 'edit'])->middleware('auth');

//# ATTENDANCE
Route::get('attendance/list', [App\Http\Controllers\Attendance\AttendanceController::class, 'list'])->name('attendance list')->middleware('auth');
Route::get('attendance/edit/{NIP}', [App\Http\Controllers\Attendance\AttendanceController::class, 'editform'])->name('attendance edit')->middleware('auth');
Route::post('attendance/edit/{NIP}', [App\Http\Controllers\Attendance\AttendanceController::class, 'edit'])->middleware('auth');


//# AUTH;
Auth::routes();
Route::get('logout', [App\Http\Controllers\Auth\LoginController::class, 'logout'])->name('logout')->middleware('auth');


//# Calendar
Route::get('full-calender',[App\Http\Controllers\FullCalendarController::class, 'index']);

Route::get('calendar', [App\Http\Controllers\FullCalendarController::class, 'index'])->name('calendar.index');
Route::post('calendar/create-event', [App\Http\Controllers\FullCalendarController::class, 'create'])->name('calendar.create');
Route::patch('calendar/edit-event', [App\Http\Controllers\FullCalendarController::class, 'edit'])->name('calendar.edit');
Route::delete('calendar/remove-event', [App\Http\Controllers\FullCalendarController::class, 'destroy'])->name('calendar.destroy');

