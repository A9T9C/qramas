<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('presensi', function (Blueprint $table) {
            $table->id('IDPRESENSI');
            $table->string('EMPLOYEEID',255);
            $table->date('WAKTU');
            $table->integer('IDLOKASI');
            $table->string('PHONEDETAIL');
            $table->integer('LATITUDE');
            $table->integer('LONGITUDE');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('presensi');
    }
};
