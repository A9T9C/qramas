<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->char('user', 255)->unique();
            $table->char('email', 255)->nullable();
            $table->char('password', 255);
            $table->char('name', 255);
            $table->tinyInteger('credentials')->nullable();
            $table->tinyInteger('statusactive');
            $table->tinyInteger('privilage');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
};
