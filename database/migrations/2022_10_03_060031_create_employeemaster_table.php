<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employeemaster', function (Blueprint $table) {
            $table->id();
            $table->char('EMPLOYEEID', 16);
            $table->char('FULLNAME', 100);
            $table->char('UNIT', 50);
            $table->char('ACTIVESTATUS', 1);
            $table->char('DEVICEID', 100)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employeemaster');
    }
};
