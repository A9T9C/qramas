<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = [
            'admin',
            'user'
        ];

        $password = [
            'adminQRAMAS',
            'userQRAMAS'
        ];

        $name = [
            'adminqramas',
            'userqramas'
        ];
        $privilage = [
            1,
            0
        ];
        for ($i=0; $i < count($name); $i++) {

            DB::table('users')->insert([
                'user' => $user[$i],
                'name' => $name[$i],
                'credentials'=>1,
                'statusactive' => 1,
                'privilage' => $privilage[$i],
                'password' => Hash::make($password[$i])
                ]);
        }
    }
}
