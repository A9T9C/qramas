<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class KehadiranSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('kehadiran')->insert([
            'status_kehadiran' => 'WFO'
        ]);

        DB::table('kehadiran')->insert([
            'status_kehadiran' => 'WFH'
        ]);

        DB::table('kehadiran')->insert([
            'status_kehadiran' => 'Cuti'
        ]);
    }
}
