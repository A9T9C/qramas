<?php

namespace App\Http\Controllers;

use Illuminate\Htpp\Request;
use App\User;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller{
    public function __construct(){
        $this->middleware('auth');
    }

    public function list(){
        $users = User::paginate(20);
        return view('app.user.userlist', compact('users'));
    }

    public function add(Request $request){
        $request->username = strtolower($request->username);
        $validator = Validator::make($request->all(), [
            'nip' => 'required|unique:users,nip|max:20',
            'username' =>'required|unique:users,username|max:50',
            'name'=> 'required',
            'position' => 'required',
            'unit' => 'required',
            'privilage' => 'required'
        ]);

        if($validator->fails){
            return redirect()
                        ->back()
                        ->withErrors($validator)
                        ->withInput();
        }
        $newUser = new User();
        $newUser -> nip = $request -> nip;
        $newUser -> username = $request -> username;
        $newUser -> name = $request -> name;
        $newUser -> position = $request -> position;
        $newUser -> unit = $request -> unit;
        $newUser -> privilege = $request -> privilege;
        $newUser -> account_status = 1;
        $newUser -> created_by = \Auth::user()->id;
        $newUser -> updated_by = 0;

        if($newUser-> save()){
            return redirect()
                        ->back()
                        ->with('success', 'Data berhasil disimpan');
        }
        else{
            return redirect()
                        ->back()
                        ->withErrors([
                            'err_msg' => 'Gagal menyimpan data, silahkan hubungi Administrator untuk informasi lebih lanjut'
                        ]);

        }


    }
    public function editForm($id){
        $user = User::find($id);
        return view('app.user.useredit', compact('user'));
    }

}
