<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = 'dashboard';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function login(Request $request){
        $input = $request->all();

        $this->validate($request, [
            'username' => 'required',
            'password' => 'required'
        ]);
        $fieldtype = filter_var($request->users, FILTER_VALIDATE_EMAIL) ? 'email' :'user';
        if(Auth::attempt(array($fieldtype => $input['username'], 'password'=> $input['password']))){
            if(Auth::user()->privilage == 1){

                return redirect()->route('dashboard');

            }else{

                return redirect()->route('user list');

            }

        }else{

            return redirect()->route('login')
                ->withErrors ('Username atau password salah.');

        }

    }
}
