
@extends('layouts.master')

@section('title', 'PRESENSI ITTS')

@section('addStyle')
@endsection

@section('content')

    <span class="mask bg-gradient-light opacity-6"  style="background-image:url({{ asset('login_background.jpg') }})">

    </span>
    <div class="container my-8">
        <div class="row">
            <div class="col-lg-5 col-md-10 mx-auto">
                <div class="card z-index-0 fadeIn3 fadeInBottom">


                    <div class="card-header bg-gradient-danger p-4 position-relative mt-n4 mx-0 z-index-2" >
                        <h4 class="text-white font-weight-bolder text-center mt-2 mb-0">LOGIN - QRAMAS </h4>

                    </div>

                    <div class="card-body">
                        <form method="POST" action="{{ route('login') }}">
                            @csrf

                            @if (count($errors) > 0)
                                <div class="alert alert-warning">
                                    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close">
                                        <i class="material-icons">close</i>
                                    </button>
                                    <span>
                                        <b> Login Failed - </b>
                                        @foreach($errors->all() as $error)
                                            {{ $error }}
                                        @endforeach
                                    </span>
                                </div>
                            @endif

                            <div class="form-group row">
                                <label for="username" class="form-floating">Username / EmployeeID</label>

                                <input id="username" type="text" class="form-control @error('username') is-invalid @enderror" name="username" value="{{ old('username') }}" required autocomplete="username" autofocus>

                            </div>

                            <div class="form-group row">
                                <label for="password" class="form-floating">Password</label>

                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">

                            </div>

                            <div class="form-group row mb-0">
                                <div class="text-center">
                                    <button type="submit" class="btn btn-danger">
                                        {{ __('Login') }}
                                    </button>
                                </div>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('addScript')
@endsection
