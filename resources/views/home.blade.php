@extends('layouts.master')

@section('title', 'Dashboard')

@section('addStyle')
@endsection

@section('navbar')
    @include('partials.navbar')
@endsection

@section('content')
<div class="container fluid py-4">
    <div class="page-title-box">
        <div class="row align-items-center">
            <div class="col-sm-6 text-left">
                <h4 class="page-title">Dashboard</h4>
            </div>
            <div class="col-sm-6">
                <div class="float-right d-none d-md-block">
                    <div class></div>
                </div>
            </div>
        </div>
    </div>
    <hr class="sidenav-divider horizontal dark shadow-xl">
        <div class="row justify-content-center mb-4">
            <div class="col-xl-3 col-md-6">
                <div class="container border-radius-lg bg-primary text-white">
                    <div class="card-body">
                        <div class="mb-4">
                            <div class="float-left mini-stat-img mr-4">
                                <span class="ti-id-badge" style="font-size: 10px"></span>
                            </div>
                            <h6 class="font-10 text-uppercase mt-0 text-white-50">Total <br> Employees</h6>
                            <h6 class="font-10">1 </h6>
                            <span class="user" style="font-size: 10px"></span>
                        </div>
                        <div class="pt-2">
                            <div class="float-right">
                                <a href="#" class="text-white-50"><i class="mdi mdi-arrow-right h5"></i></a>
                            </div>
                            <p class="text-white-50 mb-0">More info</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-3 col-md-6">
                <div class="container border-radius-lg bg-info text-white">
                    <div class="card-body">
                        <div class="mb-4">
                            <div class="float-left mini-stat-img mr-4">
                                <i class="ti-alarm-clock" style="font-size: 20px"></i>
                            </div>
                            <h6 class="font-16 text-uppercase mt-0 text-white-50">On Time <br> Percentage</h6>
                            <h4 class="font-500">0 %<i class="text-danger ml-2"></i></h4>

                        </div>
                        <div class="pt-2">
                            <div class="float-right">
                                <a href="#" class="text-white-50"><i class="mdi mdi-arrow-right h5"></i></a>
                            </div>

                            <p class="text-white-50 mb-0">More info</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-3 col-md-6">
                <div class="container border-radius-lg bg-success text-white">
                    <div class="card-body">
                        <div class="mb-4">
                            <div class="float-left mini-stat-img mr-4">
                                <i class=" ti-check-box " style="font-size: 20px"></i>
                            </div>
                            <h5 class="font-16 text-uppercase mt-0 text-white-50">On Time <br> Today</h5>
                            <h4 class="font-500">0 <i class=" text-success ml-2"></i></h4>

                        </div>
                        <div class="pt-2">
                            <div class="float-right">
                                <a href="#" class="text-white-50"><i class="mdi mdi-arrow-right h5"></i></a>
                            </div>

                            <p class="text-white-50 mb-0">More info</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-3 col-md-6">
                <div class="container border-radius-lg bg-danger text-white">
                    <div class="card-body">
                        <div class="mb-4">
                            <div class="float-left mini-stat-img mr-4">
                                <i class="ti-alert" style="font-size: 20px"></i>
                            </div>
                            <h5 class="font-16 text-uppercase mt-0 text-white-50">Late <br> Today</h5>
                            <h4 class="font-500">0<i class=" text-success ml-2"></i></h4>

                        </div>
                        <div class="pt-2">
                            <div class="float-right">
                                <a href="#" class="text-white-50"><i class="mdi mdi-arrow-right h5"></i></a>
                            </div>

                            <p class="text-white-50 mb-0">More info</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <hr class="sidenav-divider horizontal dark shadow-xl my-auto">
        <div class="row mt-4">
            <div class="col-xl-9">
                <div class="card">
                    <div class="card-body">
                        <h4 class="mt-0 header-title mb-5">Laporan Bulanan</h4>
                        <div class="row">
                            <div class="col-lg-7">
                                <div>
                                    <div id="chart-with-area" class="ct-chart earning ct-golden-section"><div class="chartist-tooltip" style="top: 24px; left: 495px;"></div><svg xmlns:ct="http://gionkunz.github.com/chartist-js/ct" width="100%" height="100%" class="ct-chart-line" style="width: 100%; height: 100%;"><g class="ct-grids"><line x1="50" x2="50" y1="15" y2="249" class="ct-grid ct-horizontal"></line><line x1="105.20703125" x2="105.20703125" y1="15" y2="249" class="ct-grid ct-horizontal"></line><line x1="160.4140625" x2="160.4140625" y1="15" y2="249" class="ct-grid ct-horizontal"></line><line x1="215.62109375" x2="215.62109375" y1="15" y2="249" class="ct-grid ct-horizontal"></line><line x1="270.828125" x2="270.828125" y1="15" y2="249" class="ct-grid ct-horizontal"></line><line x1="326.03515625" x2="326.03515625" y1="15" y2="249" class="ct-grid ct-horizontal"></line><line x1="381.2421875" x2="381.2421875" y1="15" y2="249" class="ct-grid ct-horizontal"></line><line x1="436.44921875" x2="436.44921875" y1="15" y2="249" class="ct-grid ct-horizontal"></line><line y1="249" y2="249" x1="50" x2="491.65625" class="ct-grid ct-vertical"></line><line y1="223" y2="223" x1="50" x2="491.65625" class="ct-grid ct-vertical"></line><line y1="197" y2="197" x1="50" x2="491.65625" class="ct-grid ct-vertical"></line><line y1="171" y2="171" x1="50" x2="491.65625" class="ct-grid ct-vertical"></line><line y1="145" y2="145" x1="50" x2="491.65625" class="ct-grid ct-vertical"></line><line y1="119" y2="119" x1="50" x2="491.65625" class="ct-grid ct-vertical"></line><line y1="93" y2="93" x1="50" x2="491.65625" class="ct-grid ct-vertical"></line><line y1="67" y2="67" x1="50" x2="491.65625" class="ct-grid ct-vertical"></line><line y1="41" y2="41" x1="50" x2="491.65625" class="ct-grid ct-vertical"></line><line y1="15" y2="15" x1="50" x2="491.65625" class="ct-grid ct-vertical"></line></g><g><g class="ct-series ct-series-a"><path d="M50,249L50,119C68.402,84.333,86.805,15,105.207,15C123.609,15,142.012,67,160.414,67C178.816,67,197.219,41,215.621,41C234.023,41,252.426,98.2,270.828,119C289.23,139.8,307.633,171,326.035,171C344.438,171,362.84,119,381.242,119C399.645,119,418.047,136.333,436.449,145L436.449,249Z" class="ct-area"></path><path d="M50,119C68.402,84.333,86.805,15,105.207,15C123.609,15,142.012,67,160.414,67C178.816,67,197.219,41,215.621,41C234.023,41,252.426,98.2,270.828,119C289.23,139.8,307.633,171,326.035,171C344.438,171,362.84,119,381.242,119C399.645,119,418.047,136.333,436.449,145" class="ct-line"></path><line x1="50" y1="119" x2="50.01" y2="119" class="ct-point" ct:value="5"></line><line x1="105.20703125" y1="15" x2="105.21703125" y2="15" class="ct-point" ct:value="9"></line><line x1="160.4140625" y1="67" x2="160.4240625" y2="67" class="ct-point" ct:value="7"></line><line x1="215.62109375" y1="41" x2="215.63109375" y2="41" class="ct-point" ct:value="8"></line><line x1="270.828125" y1="119" x2="270.838125" y2="119" class="ct-point" ct:value="5"></line><line x1="326.03515625" y1="171" x2="326.04515625" y2="171" class="ct-point" ct:value="3"></line><line x1="381.2421875" y1="119" x2="381.2521875" y2="119" class="ct-point" ct:value="5"></line><line x1="436.44921875" y1="145" x2="436.45921875" y2="145" class="ct-point" ct:value="4"></line></g></g><g class="ct-labels"><foreignObject style="overflow: visible;" x="50" y="254" width="55.20703125" height="20"><span class="ct-label ct-horizontal ct-end" xmlns="http://www.w3.org/2000/xmlns/" style="width: 55px; height: 20px;">1</span></foreignObject><foreignObject style="overflow: visible;" x="105.20703125" y="254" width="55.20703125" height="20"><span class="ct-label ct-horizontal ct-end" xmlns="http://www.w3.org/2000/xmlns/" style="width: 55px; height: 20px;">2</span></foreignObject><foreignObject style="overflow: visible;" x="160.4140625" y="254" width="55.20703125" height="20"><span class="ct-label ct-horizontal ct-end" xmlns="http://www.w3.org/2000/xmlns/" style="width: 55px; height: 20px;">3</span></foreignObject><foreignObject style="overflow: visible;" x="215.62109375" y="254" width="55.20703125" height="20"><span class="ct-label ct-horizontal ct-end" xmlns="http://www.w3.org/2000/xmlns/" style="width: 55px; height: 20px;">4</span></foreignObject><foreignObject style="overflow: visible;" x="270.828125" y="254" width="55.20703125" height="20"><span class="ct-label ct-horizontal ct-end" xmlns="http://www.w3.org/2000/xmlns/" style="width: 55px; height: 20px;">5</span></foreignObject><foreignObject style="overflow: visible;" x="326.03515625" y="254" width="55.20703125" height="20"><span class="ct-label ct-horizontal ct-end" xmlns="http://www.w3.org/2000/xmlns/" style="width: 55px; height: 20px;">6</span></foreignObject><foreignObject style="overflow: visible;" x="381.2421875" y="254" width="55.20703125" height="20"><span class="ct-label ct-horizontal ct-end" xmlns="http://www.w3.org/2000/xmlns/" style="width: 55px; height: 20px;">7</span></foreignObject><foreignObject style="overflow: visible;" x="436.44921875" y="254" width="55.20703125" height="20"><span class="ct-label ct-horizontal ct-end" xmlns="http://www.w3.org/2000/xmlns/" style="width: 55px; height: 20px;">8</span></foreignObject><foreignObject style="overflow: visible;" y="223" x="10" height="26" width="30"><span class="ct-label ct-vertical ct-start" xmlns="http://www.w3.org/2000/xmlns/" style="height: 26px; width: 30px;">0</span></foreignObject><foreignObject style="overflow: visible;" y="197" x="10" height="26" width="30"><span class="ct-label ct-vertical ct-start" xmlns="http://www.w3.org/2000/xmlns/" style="height: 26px; width: 30px;">1</span></foreignObject><foreignObject style="overflow: visible;" y="171" x="10" height="26" width="30"><span class="ct-label ct-vertical ct-start" xmlns="http://www.w3.org/2000/xmlns/" style="height: 26px; width: 30px;">2</span></foreignObject><foreignObject style="overflow: visible;" y="145" x="10" height="26" width="30"><span class="ct-label ct-vertical ct-start" xmlns="http://www.w3.org/2000/xmlns/" style="height: 26px; width: 30px;">3</span></foreignObject><foreignObject style="overflow: visible;" y="119" x="10" height="26" width="30"><span class="ct-label ct-vertical ct-start" xmlns="http://www.w3.org/2000/xmlns/" style="height: 26px; width: 30px;">4</span></foreignObject><foreignObject style="overflow: visible;" y="93" x="10" height="26" width="30"><span class="ct-label ct-vertical ct-start" xmlns="http://www.w3.org/2000/xmlns/" style="height: 26px; width: 30px;">5</span></foreignObject><foreignObject style="overflow: visible;" y="67" x="10" height="26" width="30"><span class="ct-label ct-vertical ct-start" xmlns="http://www.w3.org/2000/xmlns/" style="height: 26px; width: 30px;">6</span></foreignObject><foreignObject style="overflow: visible;" y="41" x="10" height="26" width="30"><span class="ct-label ct-vertical ct-start" xmlns="http://www.w3.org/2000/xmlns/" style="height: 26px; width: 30px;">7</span></foreignObject><foreignObject style="overflow: visible;" y="15" x="10" height="26" width="30"><span class="ct-label ct-vertical ct-start" xmlns="http://www.w3.org/2000/xmlns/" style="height: 26px; width: 30px;">8</span></foreignObject><foreignObject style="overflow: visible;" y="-15" x="10" height="30" width="30"><span class="ct-label ct-vertical ct-start" xmlns="http://www.w3.org/2000/xmlns/" style="height: 30px; width: 30px;">9</span></foreignObject></g></svg></div>
                                </div>
                            </div>
                            <div class="col-lg-5">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="text-center">
                                            <p class="text-muted mb-4">Bulan ini</p>
                                            <h4>124</h4>
                                            <p class="text-muted mb-5">It will be as simple as in fact it will be occidental.</p>
                                            <span class="peity-donut" data-peity="{ &quot;fill&quot;: [&quot;#02a499&quot;, &quot;#f2f2f2&quot;], &quot;innerRadius&quot;: 28, &quot;radius&quot;: 32 }" data-width="72" data-height="72" style="display: none;">0/4</span><svg class="peity" height="72" width="72"><path d="M 36 0 A 36 36 0 1 1 35.99 0 L 35.99 8 A 28 28 0 1 0 36 8" data-value="4" fill="#f2f2f2"></path></svg>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="text-center">
                                            <p class="text-muted mb-4">Bulan lalu</p>
                                            <h4>200</h4>
                                            <p class="text-muted mb-5">It will be as simple as in fact it will be occidental.</p>
                                            <span class="peity-donut" data-peity="{ &quot;fill&quot;: [&quot;#02a499&quot;, &quot;#f2f2f2&quot;], &quot;innerRadius&quot;: 28, &quot;radius&quot;: 32 }" data-width="72" data-height="72" style="display: none;">3/5</span><svg class="peity" height="72" width="72"><path d="M 36 0 A 36 36 0 1 1 14.83973091747097 65.1246117974981 L 19.542012935810757 58.65247584249853 A 28 28 0 1 0 36 8" data-value="3" fill="#02a499"></path><path d="M 14.83973091747097 65.1246117974981 A 36 36 0 0 1 35.99999999999999 0 L 35.99999999999999 8 A 28 28 0 0 0 19.542012935810757 58.65247584249853" data-value="2" fill="#f2f2f2"></path></svg>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- end row -->
                    </div>
                </div>
                <!-- end card -->
            </div>

            <div class="col-xl-3">
                <div class="card">
                    <div class="card-body">
                        <div>
                            <h4 class="mt-0 header-title mb-4">Sales Analytics</h4>
                        </div>
                        <div class="wid-peity mb-4">
                            <div class="row">
                                <div class="col-md-6">
                                    <div>
                                        <p class="text-muted">Online</p>
                                        <h5 class="mb-4">1,542</h5>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="mb-4">
                                        <span class="peity-line" data-width="100%" data-peity="{ &quot;fill&quot;: [&quot;rgba(2, 164, 153,0.3)&quot;],&quot;stroke&quot;: [&quot;rgba(2, 164, 153,0.8)&quot;]}" data-height="60" style="display: none;">6,2,8,4,3,8,1,3,6,5,9,2,8,1,4,8,9,8,2,1</span><svg class="peity" height="60" width="100%"><polygon fill="rgba(2, 164, 153,0.3)" points="0 59.5 0 20.16666666666667 11 46.388888888888886 22 7.055555555555557 33 33.27777777777778 44 39.833333333333336 55 7.055555555555557 66 52.94444444444444 77 39.833333333333336 88 20.16666666666667 99 26.72222222222222 110 0.5 121 46.388888888888886 132 7.055555555555557 143 52.94444444444444 154 33.27777777777778 165 7.055555555555557 176 0.5 187 7.055555555555557 198 46.388888888888886 209 52.94444444444444 209 59.5"></polygon><polyline fill="none" points="0 20.16666666666667 11 46.388888888888886 22 7.055555555555557 33 33.27777777777778 44 39.833333333333336 55 7.055555555555557 66 52.94444444444444 77 39.833333333333336 88 20.16666666666667 99 26.72222222222222 110 0.5 121 46.388888888888886 132 7.055555555555557 143 52.94444444444444 154 33.27777777777778 165 7.055555555555557 176 0.5 187 7.055555555555557 198 46.388888888888886 209 52.94444444444444" stroke="rgba(2, 164, 153,0.8)" stroke-width="1" stroke-linecap="square"></polyline></svg>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="wid-peity mb-4">
                            <div class="row">
                                <div class="col-md-6">
                                    <div>
                                        <p class="text-muted">Offline</p>
                                        <h5 class="mb-4">6,451</h5>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="mb-4">
                                        <span class="peity-line" data-width="100%" data-peity="{ &quot;fill&quot;: [&quot;rgba(2, 164, 153,0.3)&quot;],&quot;stroke&quot;: [&quot;rgba(2, 164, 153,0.8)&quot;]}" data-height="60" style="display: none;">6,2,8,4,-3,8,1,-3,6,-5,9,2,-8,1,4,8,9,8,2,1</span><svg class="peity" height="60" width="100%"><polygon fill="rgba(2, 164, 153,0.3)" points="0 31.735294117647058 0 10.911764705882355 11 24.79411764705882 22 3.970588235294116 33 17.852941176470587 44 42.147058823529406 55 3.970588235294116 66 28.264705882352942 77 42.147058823529406 88 10.911764705882355 99 49.088235294117645 110 0.5 121 24.79411764705882 132 59.5 143 28.264705882352942 154 17.852941176470587 165 3.970588235294116 176 0.5 187 3.970588235294116 198 24.79411764705882 209 28.264705882352942 209 31.735294117647058"></polygon><polyline fill="none" points="0 10.911764705882355 11 24.79411764705882 22 3.970588235294116 33 17.852941176470587 44 42.147058823529406 55 3.970588235294116 66 28.264705882352942 77 42.147058823529406 88 10.911764705882355 99 49.088235294117645 110 0.5 121 24.79411764705882 132 59.5 143 28.264705882352942 154 17.852941176470587 165 3.970588235294116 176 0.5 187 3.970588235294116 198 24.79411764705882 209 28.264705882352942" stroke="rgba(2, 164, 153,0.8)" stroke-width="1" stroke-linecap="square"></polyline></svg>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="">
                            <div class="row">
                                <div class="col-md-6">
                                    <div>
                                        <p class="text-muted">Marketing</p>
                                        <h5>84,574</h5>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="mb-4">
                                        <span class="peity-line" data-width="100%" data-peity="{ &quot;fill&quot;: [&quot;rgba(2, 164, 153,0.3)&quot;],&quot;stroke&quot;: [&quot;rgba(2, 164, 153,0.8)&quot;]}" data-height="60" style="display: none;">6,2,8,4,3,8,1,3,6,5,9,2,8,1,4,8,9,8,2,1</span><svg class="peity" height="60" width="100%"><polygon fill="rgba(2, 164, 153,0.3)" points="0 59.5 0 20.16666666666667 11 46.388888888888886 22 7.055555555555557 33 33.27777777777778 44 39.833333333333336 55 7.055555555555557 66 52.94444444444444 77 39.833333333333336 88 20.16666666666667 99 26.72222222222222 110 0.5 121 46.388888888888886 132 7.055555555555557 143 52.94444444444444 154 33.27777777777778 165 7.055555555555557 176 0.5 187 7.055555555555557 198 46.388888888888886 209 52.94444444444444 209 59.5"></polygon><polyline fill="none" points="0 20.16666666666667 11 46.388888888888886 22 7.055555555555557 33 33.27777777777778 44 39.833333333333336 55 7.055555555555557 66 52.94444444444444 77 39.833333333333336 88 20.16666666666667 99 26.72222222222222 110 0.5 121 46.388888888888886 132 7.055555555555557 143 52.94444444444444 154 33.27777777777778 165 7.055555555555557 176 0.5 187 7.055555555555557 198 46.388888888888886 209 52.94444444444444" stroke="rgba(2, 164, 153,0.8)" stroke-width="1" stroke-linecap="square"></polyline></svg>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
@endsection

@section('addScript')

