
@extends('layouts.master')

@section('title', 'HOME')

@section('addStyle')
@endsection

@section('content')

    <div class="mask mx-auto my-auto" style="background-image: url('{{ asset('login_background.jpg') }}')" style="background-size: cover; background-repeat:no-repeat">

        <div class="col-12 text-end px-3 my-3">
            <a class="btn btn-secondary text-uppercase text-center" href="{{ route('login') }}" target="self" style="color: white">
                Admin
            </a>
        </div>
    <div class="container-fluid text-center py-7">

            <div class="clockStyle  " id="clock">123</div>
            <div class="container-fluid text-center my-auto mx-auto">
                <a class="btn btn-success text-uppercase shadow-xs" type="submit" style="cursor: pointer;" data-bs-toggle="modal" data-bs-target="#attendancemodal">
                       {{ __('Attend') }}
                </a>
                <hr class="dropdown-divider horizontal dark">
                <a class="btn btn-danger text-uppercase shadow-xs" type="submit" style="cursor: pointer;" data-bs-toggle="modal" data-bs-target="#leavemodal">
                    Leave
                </a>
            </div>

    </div>
    </div>

<!-- Script to show clock based on device clock can be changed by installing Carbon-->

@endsection

@section('addScript')
@endsection
