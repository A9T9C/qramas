@extends('layouts.master')

@section('title', 'Calendar Event')

@section('coeStyle')
@endsection

@section('content')

    <div id='calendar'></div>

@endsection

@section('CoeScript')
@endsection
