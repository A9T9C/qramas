
@section('addStyle')

<div class="modal fade" id="leavemodal" tabindex="-1" role="dialog" aria-labelledby="modalleave" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <header class="modal-title text-uppercase" id="attendanceheader">{{ __('Attendance') }}</header>
                    <button type="button" class="btn btn-danger" data-bs-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">X</span>
                    </button>
            </div>
                    <div class="modal-body">
                        <form method="POST" action="{{ route('login') }}">
                            @csrf

                            @if (count($errors)>0)
                                <div class="alert alert-warning">
                                    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close">
                                        <i class="material-icons">close</i>
                                    </button>
                                    <span>
                                        <b> Attend Failed - </b>
                                        @foreach ($errors->all() as $error)
                                            {{ $error }}
                                        @endforeach
                                    </span>
                                </div>
                            @endif
                            <div class="form-group row">
                                <label for="username" class="form-floating">Username</label>

                                <input id="username" type="text" class="form-control @error('username') is invalid @enderror" name="username" value="{{ old('username') }}" required autocomplete="username" autofocus>
                            </div>

                            <div class="form-group row">
                                <label for="password" class="form-floating">Password</label>

                                <input id="password" type="password" class="form-control @error('password') is invalid @enderror" name="password" required autocomplete="current-password">

                            </div>
                            <!-- Possible to add another button for QR auth-->
                            <div class="form-group row mb-0">
                                <div class="text-center">
                                    <button type="submit" class="btn btn-danger text-uppercase">
                                        {{ __('Leave') }}
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
        </div>
    </div>
</div>
@yield('addScript')

