<!-- Navbar -->
<div class="navbar-nav navbar-light shadow-lg border-radius-xl z-index-3 sticky-top">
    <!-- LOGO -->
        <nav class="navbar navbar-main navbar-vertical text-center" id="navbar-blur" data-bs-toggle="false">
            <div class="navbar-item" :hover>
                <a href="{{ route('dashboard') }}" class="logo" target="self">
                    <span>
                        <h1>
                            <img src="LOGO.png" width="30" height="60">
                        </h1>
                    </span>
                </a>
            </div>
    <!-- Nav tabs-->
            <div class="nav-wrapper position-relative end-0">
                <div class="col">
                    <a href="#" class="btn btn-outline-danger mt-2 mx-4 shadow-lg text-uppercase dropdown-toggle" data-bs-toggle="dropdown">
                            <i class="ni ni-single-02 text-sm me-2"></i>
                            Employee
                    </a>
                    <a href= "{{ route('attendance list') }}">
                        <button type="submit" class="btn btn-outline-danger text-uppercase mt-2 mx-4" :hover>
                                <i class="ni ni-collection text-sm me-2"></i>
                                Attendance
                        </button>
                    </a>
                    <a href="{{ url('logout') }}">
                        <button type="submit" class="btn btn-outline-danger text-uppercase mt-2 mx-4" :hover>
                            <i class="ni ni-settings-gear-65 text-sm me-2"></i>
                            Setting
                        </button>
                    </a>
                    <a href="{{ url('logout') }}">
                        <button type="submit" class="btn btn-outline-danger text-uppercase mt-2 mx-4" >
                            <i class="ni ni-bold-left text-sm me-2"></i>
                            Leave
                        </button>
                    </a>

                </div>
            </div>
            <div class="navbar-item">
                <div class="navbar-right ms-4 mt-2 mb-2">
                    <li class="nav-item dropdown pe-2 d-flex align-items-center">
                        <a href="#" class="nav-link text-dark p-0" id="dropdownMenuButton" data-bs-toggle="dropdown" aria-expanded="false">
                            <i class="fa fa-bell cursor-pointer" aria-hidden="true"></i>
                        </a>
                    </li>
                </div>
            </div>

        </nav>


</div>
  <!-- End Navbar -->
