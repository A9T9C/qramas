<!--     Fonts and icons     -->
<link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/argon-dashboard/css/font-awesome.min.css">
<!-- Icons fontawesome can be downloaded in official web-->
<link rel="stylesheet" type="text/css" href="{{ asset('fontawesome-free-6.2.0-web/css/all.css') }}">

<!-- CSS Files -->

<link href="{{ asset('argon-dashboard/css/argon-dashboard.css') }}" rel="stylesheet">
<link href="{{ asset('argon-dashboard/css/argon-dashboard.min.css') }}" rel="stylesheet">
<link href="{{ asset('argon-dashboard/css/argon-dashboard.css.map') }}" rel="stylesheet">
<link href="{{ asset('argon-dashboard/css/nucleo-icons.css') }}" rel="stylesheet">
<link href="{{ asset('argon-dashboard/css/nucleo-svg.css') }}" rel="stylesheet">
<!-- Custom CSS -->

@yield('addStyle')
