<!-- Left side-bar start -->

<aside class="sidenav navbar bg-transparent navbar-vertical navbar-expand-xs border-0 border-radius-xl fixed-start ms-2 ps" id="sidenav-main">
    <hr class="sidenav-divider vertical dark shadow-lg my-auto">
    <div class="sidenav-header">
        <div class="row-cols-auto">

            <a href="{{ route('home') }}" class="navbar-brand text-center" target="self">
                <div class="">
                        <img src="LOGO1.png" width="30" height="60">
                </div>
                        <h4>
                            <span class="sidenav-bar-brand-text" style="color:red">QRAMAS</span>
                        </h4>
            </a>

    </div>
    <div class="sidenav-wrapper">

        <ul class="navbar-nav">
            <li class="nav-item">

                    <a class="nav-link" href="{{ route('home') }}">
                            <i class="material-icons" style="color: maroon">dashboard</i>
                        <span style="color:red">Dashboard</span>
                    </a>
            </li>
            <hr class="sidenav-divider horizontal dark my-auto">
            <li class="nav-item">

                    <a class="nav-link" href="{{ route('user list') }}">
                        <i class="material-icons" style="color: maroon">supervisor_account</i>
                        <span style="color: red">Employees</span>
                    </a>

            </li>
            <hr class="sidenav-divider horizontal dark my-auto">
            <li class="nav-item">

                    <a class="nav-link" href="{{ url('logout') }}">
                        <i class="material-icons" style="color:maroon">power_settings_new</i>
                        <span style="color: red">Logout</span>
                    </a>

            </li>
            <hr class="sidenav-divider horizontal dark my-auto">
            <li class="nav-item">
                <div class="dropdown">
                    <span style="color: red">Test</span>
                    <ul class="dropdown-menu" aria-labelledby="sidenavdropdown">
                        <li><a class="dropdown-item" href="#">Action</a></li>
                        <li><a class="dropdown-item" href="#">Action</a></li>
                        <li><a class="dropdown-item" href="#">Action</a></li>
                    </ul>
                </div>
            </li>
        </ul>
    </div>

</aside>
