<!-- All Jquery -->
<!-- Download argon-dashboard https://www.creative-tim.com/product/argon-dashboard or use any dashboard template that is available for free-->
<!-- At least the appearance is good enough, then report it-->
<!-- Add folder .vscode then create file settings.json if there is unknown rule @.... appear in problems terminal and copy this code
{
    "css.lint.unknownAtRules": "ignore"
  }
-->
<script src="{{ asset('argon-dashboard/js/core/bootstrap.bundle.min.js') }}"></script>
<script src="{{ asset('argon-dashboard/js/core/bootstrap.min.js') }}"></script>
<script src="{{ asset('argon-dashboard/js/core/popper.min.js') }}"></script>
<script src="{{ asset('argon-dashboard/js/plugins/perfect-scrollbar.min.js') }}"></script>
<script src="{{ asset('argon-dashboard/js/plugins/smooth-scrollbar.min.js') }}"></script>
<script src="{{ asset('argon-dashboard/js/plugins/Chart.extension.js') }}"></script>


<!-- Plugin for chart -->
<script src="{{ asset('argon-dashboard/js/plugins/chartjs.min.js') }}"></script>

<!-- Plugin for notif  -->
<script src="{{ asset('argon-dashboard/js/plugins/bootstrap-notify.js') }}"></script>

<!-- Plugin for validation -->
<script src="{{ asset('argon-dashboard/js/plugins/jquery.validate.min.js') }}"></script>

<!-- Control Center for Material Dashboard: parallax effects, scripts for the example pages etc -->
<script src="{{ asset('argon-dashboard/js/argon-dashboard.js') }}"></script>


@yield('addScript')
