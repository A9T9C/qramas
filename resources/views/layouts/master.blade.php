<!DOCTYPE html>
<html lang="en">

	<head>

		<meta charset="utf-8">
	    <meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="csrf-token" content="{{ csrf_token() }}" />
	    <!-- Tell the browser to be responsive to screen width -->
	    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
	    <meta name="description" content="">
	    <meta name="author" content="">
	    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	    <!-- Favicon icon -->
	    <link rel="icon" type="image/png" sizes="16x16" href="Header_Logo.png">
	    <title>QRAMAS - @yield('title')</title>

	    @include('partials.style')

	</head>

	<body class="">
		<div class="wrapper ">
            @yield('navbar')
                @yield('sidebar')


	    		<div class="main-content">
                    <div class="content">
			    	    @yield('content')
                    </div>

			        @yield('footer')
                        @include('partials.footer')

                </div>
	    </div>

	    @include('partials.script')

	</body>

</html>
